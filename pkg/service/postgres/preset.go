/*
	Copyright 2018 Carmen Chan & Tony Yip

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package postgres

import (
	"context"
	"database/sql"

	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/404busters/inventory-management/apiserver/pkg/core"
	"gitlab.com/ysitd-cloud/golang-packages/dbutils"
)

// For static type checking
var _ core.PresetService = &PresetService{}

type PresetService struct {
	Connector *dbutils.Connector
	Logger    logrus.FieldLogger
}

// Don't handle preset itemType
func (s *PresetService) List(ctx context.Context) (presets []core.Preset, err error) {
	conn, err := s.Connector.Connect(ctx)
	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}
	defer conn.Close()

	rows, err := conn.QueryContext(ctx, "SELECT id, display_name FROM preset")
	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}
	defer rows.Close()

	presets = make([]core.Preset, 0)

	for rows.Next() {
		var preset core.Preset
		if err := rows.Scan(&preset.Id, &preset.DisplayName); err != nil {
			s.Logger.Error(err)
			return nil, err
		}
		presets = append(presets, preset)
	}

	if err := rows.Err(); err != nil {
		s.Logger.Error(err)
		return nil, err
	}

	return presets, nil
}

func (s *PresetService) Get(ctx context.Context, id string) (*core.Preset, error) {
	conn, err := s.Connector.Connect(ctx)
	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}
	defer conn.Close()

	var preset core.Preset

	row := conn.QueryRowContext(ctx, "SELECT id, display_name FROM preset WHERE id = $1", id)
	if err := row.Scan(&preset.Id, &preset.DisplayName); err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	rows, err := conn.QueryContext(ctx, "SELECT item_type, require_number FROM preset_item_type WHERE preset = $1", id)
	if err == sql.ErrNoRows {
		return &preset, nil
	} else if err != nil {
		s.Logger.WithError(err).Error("Fetch Preset_item_type by preset")
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var item core.ItemTypes
		if err := row.Scan(&item.Id, &item.Count); err != nil {
			return &preset, nil
		}
		preset.ItemTypes = append(preset.ItemTypes, item)
	}

	if err := rows.Err(); err != nil {
		s.Logger.Error(err)
		return nil, err
	}

	return &preset, nil
}

func (s *PresetService) Create(ctx context.Context, input *core.Preset) (*core.Preset, error) {
	conn, err := s.Connector.Connect(ctx)
	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}
	defer conn.Close()

	var preset core.Preset

	tx, err := conn.BeginTx(ctx, nil)

	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}

	row := tx.QueryRowContext(ctx, "INSERT INTO preset (id ,display_name) VALUES ( $1, $2) RETURNING id, display_name", uuid.NewV4(), input.DisplayName)
	defer tx.Rollback()

	if err := row.Scan(&preset.Id, &preset.DisplayName); err != nil {
		s.Logger.Error(err)
		return nil, nil
	}

	preset.ItemTypes, err = insertItemTypeMap(ctx, tx, input)

	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		s.Logger.Error(err)
		return nil, err
	}

	return &preset, nil
}

func insertItemTypeMap(ctx context.Context, tx *sql.Tx, input *core.Preset) (itemTypes []core.ItemTypes, err error) {
	results := make([]core.ItemTypes, 0)
	for _, v := range input.ItemTypes {
		var item core.ItemTypes
		row := tx.QueryRowContext(ctx, "INSERT INTO preset_item_type (preset, item_type, require_number) VALUES ($1, $2, $3) RETURNING item_type, require_number", input.Id, v.Id, v.Count)

		if err := row.Scan(&item.Id, &item.Count); err != nil {
			return nil, err
		}
		results = append(results, item)
	}

	return results, nil
}

func (s *PresetService) Update(ctx context.Context, id string, input *core.Preset) (*core.Preset, error) {
	conn, err := s.Connector.Connect(ctx)
	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}
	defer conn.Close()

	var preset core.Preset

	tx, err := conn.BeginTx(ctx, nil)

	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}

	row := tx.QueryRowContext(ctx, "UPDATE preset SET display_name = $1, updated_at = current_timestamp WHERE id = $2 RETURNING id, display_name", input.DisplayName, id)
	defer tx.Rollback()

	if err := row.Scan(&preset.Id, &preset.DisplayName); err != nil {
		s.Logger.Error(err)
		return nil, nil
	}

	result, err := tx.Exec("DELETE FROM preset_item_type WHERE item_type = $1", id)
	if err != nil {
		return nil, err
	}

	cnt, err := result.RowsAffected()
	if err != nil {
		return nil, err
	} else if cnt < 1 {
		return nil, core.ErrRecordNotExists
	}

	preset.ItemTypes, err = insertItemTypeMap(ctx, tx, input)

	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		s.Logger.Error(err)
		return nil, err
	}

	return &preset, nil
}

func (s *PresetService) Delete(ctx context.Context, id string) error {
	conn, err := s.Connector.Connect(ctx)
	if err != nil {
		s.Logger.Error(err)
		return err
	}
	defer conn.Close()

	tx, err := conn.BeginTx(ctx, nil)

	if err != nil {
		s.Logger.Error(err)
		return err
	}

	result, err := tx.Exec("DELETE FROM preset_item_type WHERE item_type = $1", id)
	defer tx.Rollback()

	if err != nil {
		return err
	}

	cnt, err := result.RowsAffected()
	if err != nil {
		s.Logger.Error(err)
		return err
	} else if cnt < 1 {
		return core.ErrRecordNotExists
	}
	result, err = tx.Exec("DELETE FROM preset WHERE id = $1", id)

	if err != nil {
		s.Logger.Error(err)
		return err
	}

	cnt, err = result.RowsAffected()
	if err != nil {
		s.Logger.Error(err)
		return err
	} else if cnt < 1 {
		return core.ErrRecordNotExists
	}

	if err := tx.Commit(); err != nil {
		s.Logger.Error(err)
		return err
	}

	return nil
}
