package postgres

import (
	"context"
	"database/sql"
	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/404busters/inventory-management/apiserver/pkg/core"
	"gitlab.com/ysitd-cloud/golang-packages/dbutils"
)

var _ core.TransportService = &TransportService{}

type TransportService struct {
	Connector *dbutils.Connector
	Logger    logrus.FieldLogger
}

func (s *TransportService) Get(ctx context.Context, id string) (_ *core.Transport, err error) {
	conn, err := s.Connector.Connect(ctx)
	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}
	defer conn.Close()

	var transport core.Transport

	row := conn.QueryRowContext(ctx, "SELECT  id, person_in_charge, location, event_type, notes FROM transport WHERE id = $1 AND deleted_at IS NOT NULL ", id)
	if err := row.Scan(&transport.Id); err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	const inventoryQuery = "SELECT inventory FROM transport_inventory WHERE transport = $1"
	rows, err := conn.QueryContext(ctx, inventoryQuery, id)
	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}
	defer rows.Close()

	transport.Inventories = make([]string, 0)

	for rows.Next() {
		var inventory string
		if err := rows.Scan(&inventory); err != nil {
			return nil, err
		}
		transport.Inventories = append(transport.Inventories, inventory)
	}

	if err := rows.Err(); err != nil {
		s.Logger.Error(err)
		return nil, err
	}

	return &transport, nil
}

func (s *TransportService) List(ctx context.Context) ([]core.Transport, error) {
	conn, err := s.Connector.Connect(ctx)
	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}
	defer conn.Close()

	rows, err := conn.QueryContext(ctx, "SELECT id, person_in_charge, location, event_type, notes FROM transport WHERE deleted_at IS NULL")
	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}
	defer rows.Close()

	var transportList = make([]core.Transport, 0)
	for rows.Next() {
		var transport core.Transport
		if err := rows.Scan(&transport.Id, &transport.PersonInCharge, &transport.Location, &transport.EventType, &transport.Note); err != nil {
			s.Logger.Error(err)
			return nil, err
		}
		transportList = append(transportList, transport)
	}

	if err := rows.Err(); err != nil {
		s.Logger.Error(err)
		return nil, err
	}

	return transportList, nil
}

func (s *TransportService) FilterList(ctx context.Context, filter core.TransportFilterOption) ([]core.Transport, error) {
	conn, err := s.Connector.Connect(ctx)
	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}
	defer conn.Close()

	var referenceQuery string
	var referenceId string
	var searchQuery string
	var errorStr string
	if len(filter.User) > 0 && len(filter.Inventory) > 0 || len(filter.User) == 0 && len(filter.Inventory) == 0 {
		return nil, core.ErrInvaildInput
	} else if len(filter.User) > 0 {
		referenceQuery = "SELECT name FROM inventory WHERE id = $1"
		referenceId = filter.User
		searchQuery = "SELECT id, person_in_charge, location, event_type, notes FROM transport WHERE person_in_charge = $1 AND deleted_at IS NULL"
		errorStr = "Fetch transport by user"
	} else if len(filter.Inventory) > 0 {
		referenceQuery = "SELECT id FROM inventory WHERE id = $1"
		referenceId = filter.Inventory
		searchQuery = "SELECT id, person_in_charge, location, event_type, notes FROM transport t inner join " +
			"transport_inventory ti on t.id = ti.transport " +
			"WHERE ti.inventory = $1 and t.deleted_at isnull and ti.deleted_at isnull "
		errorStr = "Fetch transport by inventory"
	}

	result, err := conn.ExecContext(ctx, referenceQuery, referenceId)
	if err != nil {
		s.Logger.Error(err)
		return nil, err
	}

	cnt, err := result.RowsAffected()
	if err != nil {
		s.Logger.Error(err)
		return nil, err
	} else if cnt < 1 {
		return nil, core.ErrReferenceNotExists
	}

	rows, err := conn.QueryContext(ctx, searchQuery)
	defer rows.Close()

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		s.Logger.WithError(err).Error(errorStr)
		return nil, err
	}

	var transportList = make([]core.Transport, 0)

	for rows.Next() {
		var transport core.Transport
		if err := rows.Scan(&transport.Id, &transport.PersonInCharge, &transport.Location, &transport.EventType, &transport.Note); err != nil {
			s.Logger.Error(err)
			return nil, err
		}
		transportList = append(transportList, transport)
	}

	if err := rows.Err(); err != nil {
		s.Logger.Error(err)
		return nil, err
	}

	return transportList, nil
}

func (s *TransportService) CheckIn(ctx context.Context, input *core.Transport) (*core.Transport, error) {
	if input.EventType != core.EventCheckIn {
		return nil, core.ErrInvaildInput
	}
	return s.addRecord(ctx, input)
}

func (s *TransportService) CheckOut(ctx context.Context, input *core.Transport) (*core.Transport, error) {
	if input.EventType != core.EventCheckOut {
		return nil, core.ErrInvaildInput
	}
	return s.addRecord(ctx, input)
}

func (s *TransportService) addRecord(ctx context.Context, input *core.Transport) (_ *core.Transport, err error) {
	conn, err := s.Connector.Connect(ctx)
	if err != nil {
		s.Logger.Error(err)
		return
	}
	defer conn.Close()

	tx, err := conn.BeginTx(ctx, nil)
	if err != nil {
		s.Logger.Error(err)
		return
	}
	defer tx.Rollback()

	input.Id = uuid.NewV4().String()
	const createRecordQuery = "INSERT INTO transport (id, person_in_charge, location, event_type, notes) VALUES ($1, $2, $3, $4, $5)"
	if _, err = tx.ExecContext(ctx, createRecordQuery, input.Id, input.PersonInCharge, input.Location, input.EventType, input.Note); err != nil {
		s.Logger.Error(err)
		return
	}

	const addInventoryQuery = "INSERT INTO transport_inventory (transport, inventory) VALUES ($1, $2)"
	stmt, err := tx.PrepareContext(ctx, addInventoryQuery)
	if err != nil {
		s.Logger.Error(err)
		return
	}
	defer stmt.Close()
	for _, inventory := range input.Inventories {
		if _, err = stmt.ExecContext(ctx, input.Id, inventory); err != nil {
			s.Logger.Error(err)
			return
		}
	}

	const updateStatusQuery = "UPDATE inventory SET status = $1 WHERE id = $2"
	updateStmt, err := tx.PrepareContext(ctx, updateStatusQuery)
	if err != nil {
		s.Logger.Error(err)
		return
	}
	defer updateStmt.Close()
	status := core.StatusTransport
	if input.EventType == core.EventCheckIn {
		status = core.StatusStock
	}
	for _, inventory := range input.Inventories {
		if _, err = updateStmt.ExecContext(ctx, status, inventory); err != nil {
			s.Logger.Error(err)
			return
		}
	}

	if err = tx.Commit(); err != nil {
		s.Logger.Error(err)
		return
	}

	return input, nil
}
