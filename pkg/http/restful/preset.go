/*
	Copyright 2018 Carmen Chan & Tony Yip

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package restful

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/404busters/inventory-management/apiserver/pkg/core"
)

type presetHandler struct {
	Service core.PresetService
}

func (h *presetHandler) get(c *gin.Context) {
	id := c.Param("id")
	preset, err := h.Service.Get(c.Request.Context(), id)
	if err != nil {
		c.JSON(http.StatusServiceUnavailable, ErrorRes{
			Code:    "database_error",
			Message: err.Error(),
		})
	} else if preset == nil {
		c.JSON(http.StatusNotFound, ErrorRes{
			Code:    "item_not_found",
			Message: fmt.Sprintf("Preset %s is not exists", id),
		})
		return
	} else {
		c.JSON(http.StatusOK, ApiRes{
			Data: preset,
		})
	}
}

func (h *presetHandler) create(c *gin.Context) {
	var presetInput core.Preset
	err := c.ShouldBindJSON(&presetInput)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, ErrorRes{
			Code:    "invalid_input",
			Message: err.Error(),
		})
		return
	}

	preset, err := h.Service.Create(c.Request.Context(), &presetInput)
	if err != nil {
		c.JSON(http.StatusServiceUnavailable, ErrorRes{
			Code:    "database_error",
			Message: err.Error(),
		})
	} else {
		c.JSON(http.StatusOK, ApiRes{
			Data: preset,
		})
	}
}

func (h *presetHandler) update(c *gin.Context) {
	id := c.Param("id")
	var presetInput core.Preset
	err := c.ShouldBindJSON(&presetInput)

	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, ErrorRes{
			Code:    "invalid_input",
			Message: err.Error(),
		})
		return
	}

	preset, err := h.Service.Update(c.Request.Context(), id, &presetInput)
	if err == nil && preset == nil {
		c.JSON(http.StatusNotFound, ErrorRes{
			Code:    "item_not_Found",
			Message: fmt.Sprintf("preset %s is not exists", id),
		})
	} else if err != nil {
		c.JSON(http.StatusServiceUnavailable, ErrorRes{
			Code:    "database_error",
			Message: err.Error(),
		})
	} else {
		c.JSON(http.StatusOK, ApiRes{
			Data: preset,
		})
	}
}

func (h *presetHandler) delete(c *gin.Context) {
	id := c.Param("id")

	if err := h.Service.Delete(c.Request.Context(), id); err == core.ErrRecordNotExists {
		c.JSON(http.StatusNotFound, ErrorRes{
			Code:    "item_not_Found",
			Message: fmt.Sprintf("preset %s is not exists", id),
		})
	} else if err != nil {
		c.JSON(http.StatusServiceUnavailable, ErrorRes{
			Code:    "database_error",
			Message: err.Error(),
		})
	} else {
		c.Status(http.StatusOK)
	}
}
