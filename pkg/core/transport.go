package core

import "context"

type Transport struct {
	Id             string    `json:"id,omitempty"`
	PersonInCharge string    `json:"person_in_charge" graphql:"-"`
	Location       string    `json:"location" graphql:"-"`
	EventType      EventType `json:"event_type,omitempty"`
	Note           string    `json:"note"`
	Inventories    []string  `json:"inventories,omitempty"`
}

type EventType string

const (
	EventCheckIn  EventType = "CHECK_IN"
	EventCheckOut EventType = "CHECK_OUT"
)

type TransportFilterOption struct {
	User      string
	Inventory string
}

type TransportService interface {
	Get(ctx context.Context, id string) (_ *Transport, err error)
	List(ctx context.Context) ([]Transport, error)
	FilterList(ctx context.Context, filter TransportFilterOption) ([]Transport, error)
	CheckIn(ctx context.Context, input *Transport) (*Transport, error)
	CheckOut(ctx context.Context, input *Transport) (*Transport, error)
}
