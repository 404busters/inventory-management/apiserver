FROM golang:1.12-alpine AS builder

ENV CGO_ENABLED 0

WORKDIR /go/apiserver

RUN apk add --no-cache git

COPY . .

RUN go install -v gitlab.com/404busters/inventory-management/apiserver/cmd/apiserver

FROM gcr.io/distroless/static

ENV PORT 8080

COPY --from=builder /go/bin/apiserver /

CMD ["/apiserver"]
