// +heroku install gitlab.com/404busters/inventory-management/apiserver/cmd/apiserver

module gitlab.com/404busters/inventory-management/apiserver

require (
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/gorilla/handlers v1.4.0
	github.com/gorilla/websocket v1.4.0 // indirect
	github.com/graphql-go/graphql v0.7.7 // indirect
	github.com/lib/pq v1.0.0
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/samsarahq/go v0.0.0-20190126203740-720caea591c9 // indirect
	github.com/samsarahq/thunder v0.5.0
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.4.0
	github.com/ugorji/go/codec v0.0.0-20190315113641-a70535d8491c // indirect
	gitlab.com/ysitd-cloud/golang-packages/dbutils v1.0.0
	golang.org/x/sync v0.0.0-20190227155943-e225da77a7e6 // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
