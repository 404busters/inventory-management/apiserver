CREATE TABLE inventory.preset
(
  id uuid PRIMARY KEY NOT NULL,
  display_name text,
  created_at timestamp without time zone not null default current_timestamp,
  updated_at timestamp without time zone not null default current_timestamp,
  deleted_at timestamp without time zone
);
CREATE UNIQUE INDEX preset_id_uindex ON inventory.preset (id);

CREATE TABLE preset_item_type
(
  preset uuid,
  item_type uuid,
  require_number integer,
  CONSTRAINT preset_item_type_pk PRIMARY KEY (preset, item_type),
  CONSTRAINT preset_item_type_preset_id_fk FOREIGN KEY (preset) REFERENCES inventory.preset (id),
  CONSTRAINT preset_item_type_item_type_id_fk FOREIGN KEY (item_type) REFERENCES inventory.item_type (id)
);
CREATE UNIQUE INDEX preset_item_type_item_type_uindex ON preset_item_type (item_type);
CREATE UNIQUE INDEX preset_item_type_preset_uindex ON preset_item_type (preset);
